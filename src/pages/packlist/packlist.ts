import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { CloudClosetPage } from '../cloud-closet/cloud-closet';
/*
  Generated class for the Packlist page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-packlist',
  templateUrl: 'packlist.html'
})
export class PacklistPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {}
show(){
  alert('danish');
}

showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Add New Packlist',
      message: "",
      inputs: [
        {
          name: 'Name',
          placeholder: 'Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }

goToCloud(){
  this.navCtrl.push(CloudClosetPage);
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad PacklistPage');
  }

}
