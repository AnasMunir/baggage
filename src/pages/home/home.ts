import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { ClosetPage } from '../closet/closet';
import { DoorDoorPage } from '../door-door/door-door';
import { BaggageStoragePage } from '../baggage-storage/baggage-storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
    
  }

  goToCloset(){
    this.navCtrl.push(ClosetPage);
  }

  goToDoor(){
    this.navCtrl.push(DoorDoorPage);
  }
  goToBaggage(){
    this.navCtrl.push(BaggageStoragePage);
  }

}
