import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the DoorDoor page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-door-door',
  templateUrl: 'door-door.html'
})
export class DoorDoorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}
 public event = {
    pickmonth: '2017-04-01',
    arrivalmonth: '2017-04-01',
    timeStarts: '07:43',
    timeEnds: '1990-02-20'
 }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DoorDoorPage');
  }

}
