import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'page-baggage-storage',
  templateUrl: 'baggage-storage.html'
})
export class BaggageStoragePage {
  submitAttempt: boolean = false;
  baggageStorageForm: FormGroup
  
  visible: boolean = false;

  public event = {
    pickmonth: '2017-04-01',
    arrivalmonth: '2017-04-01',
    timeStarts: '07:43',
    timeEnds: '1990-02-20'
  }
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder) { 

      this.baggageStorageForm = formBuilder.group({
        noOfSmallBags: [''],
        noOfMediumBags: [''],
        noOfLargeBags: [''],
        noOfDaysStorage: [''],
        pickupService: ['false'],
        nameOfAirline: [{value: '', disabled: true}],
        flightNumber: [{value: '', disabled: true}],
        timeOfArrival: [{value: '', disabled: true}],
        flightType: [{value: 'domestic', disabled: true}],
        returnLuggageDeliveryService: ['false'],
        dateOfService: [''],
        earlySpecialStorgeFees: [''],
        lateSpecialStorgeFees: ['']
      })
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BaggageStoragePage');
  }
  pickupService($event) {
    console.log($event);
    console.log($event._checked);
    if($event._checked === true) {
      this.visible = true;
    } else {
      this.visible = false;
    }
  }
}
