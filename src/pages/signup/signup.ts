import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../../providers/auth-service';
import { EmailValidator } from '../../validators/email';

import { HomePage } from '../home/home';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  submitAttempt: boolean = false;
  signupForm: FormGroup

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public auth: AuthService) {

    this.signupForm = formBuilder.group({
      firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      phone: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(10), Validators.pattern('[0-9]*'), Validators.required])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      promo: ['']
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  signup() {
    this.submitAttempt = true;
    if (!this.signupForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      console.log('form validated');
      console.log(this.signupForm.value);
      this.auth.signupUser(
        this.signupForm.value.email,
        this.signupForm.value.password,
        this.signupForm.value)
        .then(data => {
          console.log(data);
          this.navCtrl.setRoot(HomePage);

        }, (error) => {
          console.error(error);
        });
    }
  }
}
