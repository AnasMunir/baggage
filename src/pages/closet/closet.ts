import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PacklistPage } from '../packlist/packlist';

/*
  Generated class for the Closet page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-closet',
  templateUrl: 'closet.html'
})
export class ClosetPage {
options: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
 this.options = "Clothing";
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ClosetPage');
  }

  goToPacklist(){
    this.navCtrl.push(PacklistPage);
    // console.log('im deing clicked');
  }

}
