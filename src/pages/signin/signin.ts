import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../../providers/auth-service';
import { EmailValidator } from '../../validators/email';

import { HomePage } from '../home/home';

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})
export class SigninPage {
  public submitAttempt: boolean = false;
  public loginForm: FormGroup;


  constructor(public navCtrl: NavController,
    public formBuilder: FormBuilder,
    // public loadingCtrl: LoadingController,
    // public alertCtrl: AlertController,
    public auth: AuthService) {

    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
  }

  login() {
    this.submitAttempt = true;
    if (!this.loginForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      console.log('success!');
      console.log(this.loginForm.value);
      this.auth.loginUser(
        this.loginForm.value.email,
        this.loginForm.value.password, ).then(data => {
          console.log(data);
          this.navCtrl.setRoot(HomePage);
          alert('Successfully logged in');
        }, (error) => {
          console.error(error);
        });
    }
  }

}
