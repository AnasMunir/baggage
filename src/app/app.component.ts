import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
// import { ClosetPage } from '../pages/closet/closet';
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
// import { SigninPage } from '../pages/signin/signin';
import { LandingPage } from '../pages/landing/landing';
// import { PacklistPage } from '../pages/packlist/packlist';
// import { CloudClosetPage } from '../pages/cloud-closet/cloud-closet';
// import { BaggageStoragePage } from '../pages/baggage-storage/baggage-storage';
// import { DoorDoorPage } from '../pages/door-door/door-door';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, af: AngularFire) {
    const authObserver = af.auth.subscribe(user => {
      if (user) {
        this.rootPage = HomePage;
        authObserver.unsubscribe();
      } else {
        this.rootPage = LandingPage;
        authObserver.unsubscribe();
      }
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
