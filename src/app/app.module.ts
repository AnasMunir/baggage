import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// importing pages
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { SigninPage } from '../pages/signin/signin';
import { ClosetPage } from '../pages/closet/closet';
import { LandingPage } from '../pages/landing/landing';
import { PacklistPage } from '../pages/packlist/packlist';
import { CloudClosetPage } from '../pages/cloud-closet/cloud-closet';
import { BaggageStoragePage } from '../pages/baggage-storage/baggage-storage';
import { DoorDoorPage } from '../pages/door-door/door-door';

// importing providers
import { AuthService } from '../providers/auth-service';

// Importing AF2 Module
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

// AF2 Settings
const firebaseConfig = {
  apiKey: "AIzaSyCU4XAiyx1AXOLDCfHfcRWfwkT5YDOMkT4",
  authDomain: "baggage-22a1d.firebaseapp.com",
  databaseURL: "https://baggage-22a1d.firebaseio.com",
  projectId: "baggage-22a1d",
  storageBucket: "baggage-22a1d.appspot.com",
  messagingSenderId: "215421379533"
};

const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SigninPage,
    SignupPage,
    LandingPage,
    ClosetPage,
    PacklistPage,
    CloudClosetPage,
    BaggageStoragePage,
    DoorDoorPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SigninPage,
    SignupPage,
    LandingPage,
    ClosetPage,
    PacklistPage,
    CloudClosetPage,
    BaggageStoragePage,
    DoorDoorPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthService
  ]
})
export class AppModule { }
